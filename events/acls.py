from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": city + ", " + state, "per_page": 1}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    return {"picture_url": content["photos"] [0] ["src"] ["original"]}


def get_weather(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {"q": {city, state}, "appid": OPEN_WEATHER_API_KEY}
    geo = requests.get(url, params=params)
    try:
        if geo.status_code == 200:
            geo = geo.json()
            params2 = {
                "lat": geo [0] ["lat"],
                "lon": geo [0] ["lon"],
                "units": "imperial",
                "appid": OPEN_WEATHER_API_KEY,
            }
    except IndexError:
        return ["none","none"]

    w_url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(w_url, params=params2)
    response = response.json()
    description = response["weather"] [0] ["description"]
    temp = response["main"] ["temp"]
    return description, temp
